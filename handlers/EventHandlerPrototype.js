module.exports = EventHandler;

function EventHandler(config){
    Object.assign(this,config);
}
EventHandler.prototype.getHighwater = function(){
    return this._highwater;
};

EventHandler.prototype.initialize = function(){
    return Promise.resolve();
};

EventHandler.prototype.getEventId = function(row){
    return row.id;
};

EventHandler.prototype.pause = function(){
    this.paused = true
}

EventHandler.prototype.unpause = function(){
    this.paused = false
}


EventHandler.prototype.cycle = function(waitMS){
    setTimeout(this.loop, waitMS)
}
