'use strict';
const _ = require('highland');

const EventHandlerPrototype = require('./EventHandlerPrototype');

module.exports = BatchEventHandler;

function BatchEventHandler(base, dependencies){
    Object.assign(this,base);
    const inject = {};
    if(base.inject && process.env.NODE_ENV !== 'test'){
        base.inject.forEach( key => {
            if(!dependencies[key]){
                throw new Error("Could not load dependency `"+ key +"``")
            }
            inject[key] = dependencies[key];
        })
    }

    this.handler = function(stream, overrides) {
        if(process.env.NODE_ENV === 'test'){
            validateTestInjections(base.inject, inject, overrides)
        }

        return base.handler(stream, inject)
            .map(function(result){
                return {
                    success: true,
                    data: result
                }
            });
    };

    this.getRawHandler = function(stream){
        return base.handler(stream);
    }

}
BatchEventHandler.prototype = new EventHandlerPrototype();


function validateTestInjections(expect, inject, overrides){
    if(!overrides && expect){
        throw new Error(`Expecting to have dependencies injected into handler (${expect}) `);
    }


    if(overrides){
        Object.keys(overrides).forEach( key => {
            inject[key] = overrides[key];
        })
    }
}
