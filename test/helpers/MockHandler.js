const sinon = require('sinon');

const BatchEventHandler = require('../../handlers/BatchEventHandler');

module.exports = new BatchEventHandler({
    batchSize: 1,
    key: 'mock',
    event: 'mock',
    handler: handler,
    _stub: sinon.stub()
});

function handler(stream, inject){
    return stream
        .tap(_stub)
} 
