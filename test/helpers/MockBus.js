const sinon = require('sinon');

module.exports = function MockBus(config){
    return  {
        generator: sinon.stub().returns(Promise.resolve({ records: [], maxId: 0})),
        getEvents: sinon.stub().returns(Promise.resolve([])),
        stubHighwater: sinon.stub(),
        getOrStubHighwater: sinon.stub().returns(Promise.resolve({})),
        persistHighwater: sinon.stub().returns(Promise.resolve({}))
    };

}
