const _ = require('highland')
const mocha = require('mocha')
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect;
chai.use(require('sinon-chai'))

const EventProcessor = require('../../processors/EventProcessor');
const MockBus = require('../helpers/MockBus');
const MockHandler = require('../helpers/MockHandler');

describe('iteration', () => {
    let processor, handler, stub
    before( () => {

        processor = new EventProcessor({
            bus: MockBus()
        })

        stub = sinon.stub()

        records = [
            { id: 1 },
            { id: 2 },
            { id: 3 }
        ]

        handler = {
            cycle: () => {},
            paused: false,
            key: 'Test Handler',
            batchSize: 1,
            handler: (stream) => {
                return stream
                    .map( event => {
                        stub(event.id)
                        if(event.id === 3) throw new Error("intentional error")
                        else return event
                    })
            },
            state: {
                lastId: 0,
                cooldown: 0
            },
            generator: genStub()
        }


    })

    function * genStub(){
        yield Promise.resolve({ records: [{ id: 1 }, { id: 2}] })
        yield Promise.resolve({ records: [{ id: 3}, {id: 4}] })
        throw new Error("unexpected generator call")
    }

    it('should process a cycle of records', (done) => {

        _([1,2])
            .flatMap( () => processor._iteration(handler))
            .done( () => {

                expect(stub).to.have.callCount(3)
                done()
            })


    })
})
