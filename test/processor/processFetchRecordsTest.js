const mocha = require('mocha')
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect;
chai.use(require('sinon-chai'))

const EventProcessor = require('../../processors/EventProcessor');
const MockBus = require('../helpers/MockBus');
const MockHandler = require('../helpers/MockHandler');

describe('processFetchedRecords', () => {
    let processor, handler, stub
    before( () => {

        processor = new EventProcessor({
            bus: MockBus()
        })

        stub = sinon.stub()

        records = [
            { id: 1 },
            { id: 2 },
            { id: 3 },
            { id: 4 },
            { id: 5 },
            { id: 6 }
        ]

        handler = {
            key: 'Test Handler',
            batchSize: 3,
            handler: (stream) => {
                return stream
                    .map( event => {
                        stub(event.id)
                        if(event.id === 5) throw new Error("intentional error")
                        else return event
                    })
            },
            state: {
                lastId: 0
            }
        }


    })

    it('should pause on errors', (done) => {
        processor._processFetchedRecords(records, handler)
            .errors( (err, push) => {
                push(null, { rescued: true })
            })
            .toArray( results => {
                // stub should not have been called for id=6
                expect(stub).to.have.callCount(5)
                done()
            })

    })
})
