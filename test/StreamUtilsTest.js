'use strict';
const fs = require('fs');
const chai = require('chai');
const sinon = require('sinon');
chai.use(require("sinon-chai"));
const expect = chai.expect;

const _ = require('highland');

const StreamUtils = require('../utils/StreamUtils');

describe('StreamUtilsTest', function(){

    let clock, stub, genStub;
    beforeEach(() => {
        clock = sinon.useFakeTimers();
        stub = sinon.stub().returns( _([-1]) );
        stub.onCall(0).returns( _([1]) );
        stub.onCall(1).returns( _([2]) );
        stub.onCall(2).returns( _([3]) );
        stub.onCall(3).returns( _([4]) );
        genStub = () => stub()
    });
    afterEach( () => {
        clock.restore();
    });



    it('join should be called on intervals and return the latest tuple', function(done) {

        const stream = _( (push) => {
            push(null, 1)
            push(null, 2)
            push(null, 3)
            push(null, 4)
            push(null, _.nil);
        })
        .tap( () => clock.tick(500))
        .zip( StreamUtils.join( genStub, 1000 ) )
        .map( arr => {
            const input = arr[0];
            const table = arr[1];

            return arr[1];
        })
        .toArray( results => {
            expect(results).to.eql([1, 2, 2, 3])
            expect(stub).to.have.been.calledThrice
            done();
        });


    });

});
