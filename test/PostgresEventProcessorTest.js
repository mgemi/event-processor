const sinon = require('sinon');

const EventProcessor = require('../processors/EventProcessor');
const MockBus = require('./helpers/MockBus');
const MockHandler = require('./helpers/MockHandler');

describe('PostgresEventProcessor', () => {

    it('use database', () => {

        const processor = new EventProcessor({
            bus: 'pg',
            connection: {
                host: 'localhost',
                database: 'events_test'
            }
        });

    });

});
