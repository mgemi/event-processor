const pgp = require('pg-promise')();

module.exports = function PostgresBusFactory(config){
    const { logger, connection } = config;

    const db = pgp(connection);

    return  {
        generator: generator,
        stubHighwater: stubHighwater,
        getOrStubHighwater: getOrStubHighwater,
        persistHighwater: persistHighwater,
        lag: lag,
        addDeadletter: addDeadletter,
        getDeadletter: getDeadletter,
        removeDeadletter: removeDeadletter
    };

    function generator(handler){
        const batchQuery = "SELECT * FROM events WHERE id > $1 AND type = Any($2::varchar[]) ORDER BY id LIMIT $3";

        return db.tx(t => {
            const rowsPromise = t.any(batchQuery, [handler.state.lastId, [].concat(handler.event), handler.state.fetchSize])
            const maxIdPromise = t.oneOrNone("SELECT max(id) as max from events")
            return Promise.all([rowsPromise, maxIdPromise])
              .then( arr => {
                  const rows = arr[0]
                  const maxId = arr[1] ? arr[1].max : 0
                  return { records: rows , maxId: maxId }
              })
       })
    }

    function stubHighwater(key){
        logger.info("stub",key);
        const insert = "insert into highwater values ('event-processor',$1,$2,$3)";
        return db.any({text: insert, values: [key,'{\"LAST_ID\": 0}',new Date()] });
    }

    function getOrStubHighwater(key){
        const query = "SELECT * FROM highwater WHERE source = 'event-processor' and key = $1";
        return db.oneOrNone({ text: query, values: [key] })
            .then(result => {
                if(result ===  null){
                    return stubHighwater(key).then(() => 0);
                } else {
                    return result.data.LAST_ID;
                }
            })
            .catch(err => {
                logger.error("Error getting highwater - " + key, err, query);
                throw err;
            })
    }


    function persistHighwater(key,max){
        var insert = "update highwater set data = $1, last_run = $2 WHERE source = 'event-processor' and key = $3";
        return db.any({ text: insert, values: ['{\"LAST_ID\": '+max+'}',new Date(),key ]})
            .then(() => [key,max])
            .catch(err => {
                logger.error("Error peristing highwater", err);
                throw err;
            })
            ;
    }

    function lag(handler){
        const highwaterQuery = "SELECT (data->>'LAST_ID')::int as last_id FROM highwater h WHERE key = $1 and source = 'event-processor'";
        const metricsQuery = `
            SELECT
            max(id) as max,
            count(*) as count
            FROM
            events e
            WHERE
            id > $1
            AND type = Any($2::varchar[])
        `;

        const key = handler.key;
        const eventNames = [].concat(handler.event);
        return db.one({ text: highwaterQuery, values: [key] })
            .then(result => result.last_id !== undefined ? db.one({ text: metricsQuery, values: [result.last_id, eventNames] }) : {})
            .then( result => result.count ? { metric: handler.key + '.lag', points: parseInt(result.count) } : {})
    }

    function addDeadletter(processor, event) {
      const addDeadletterQuery = "insert into event_processor_deadletters (processor_name, event) values ($1, $2)"
      return db.any({text: addDeadletterQuery, values: [processor, JSON.stringify(event)]})
          .catch(err => {
            logger.error("Failed to add to dead letter queue", err);
            throw err;
          });
    }

    function getDeadletter(handlerKey, deadletterId) {
      let queryArgs = [handlerKey];
      let deadletterQuery = 'SELECT * FROM event_processor_deadletters WHERE processor_name = $1';
      if (deadletterId) {
        queryArgs.push(deadletterId);
        deadletterQuery += ' AND id = $2';
      }
      deadletterQuery += ' ORDER BY id asc limit 1';
      return db.oneOrNone({ text: deadletterQuery, values: queryArgs });
    }

    function removeDeadletter(deadletterId) {
      const removeDeadletterQuery = 'DELETE FROM event_processor_deadletters WHERE id = $1';
      return db.none(removeDeadletterQuery, [deadletterId]);
    }
}
