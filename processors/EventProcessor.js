const _ = require('highland');
const buses = require('./buses');
const EventHandlerPrototype = require('../handlers/EventHandlerPrototype');
const requireDirectory = require('require-directory');

module.exports = EventProcessor;

function EventProcessor(config){
    let _bus, _streams, logger;
    validate();
    configure();

    return {
        initialize: initialize,
        streams: _streams,
        register: register,
        pausedStreams: pausedStreams,
        metrics: metrics,
        logPaused: logPaused,
        _processFetchedRecords: processFetchedRecords,
        _validate: validate,
        _configure: configure,
        _initialize: initialize,
        _register: register,
        _factory: factory,
        _iteration: iteration,
        replayDeadletter: replayDeadletter
    }

    function validate(){
        if(!config.bus) throw new Error("`bus` not specified in config. Acceptable values are `pg`");
        config.checkIntervalMs = config.checkIntervalMs || 500;
        config.FETCH_SIZE = config.FETCH_SIZE || 1000
        config.BATCH_SIZE = config.BATCH_SIZE || 1
        config.MIN_COOLDOWN = config.MIN_COOLDOWN || 10
        config.MAX_COOLDOWN = config.MAX_COOLDOWN || 10 * 1000
        config.MAX_RETRIES = config.MAX_RETRIES || 5
        config.RETRY_BACKOFF = config.RETRY_BACKOFF || 60 * 1000
        config.MAX_CONSECUTIVE_ERRORS = config.MAX_CONSECUTIVE_ERRORS || 3
        if(config.MIN_COOLDOWN < 1) throw new Error("MIN_COOLDOWN must be greater than 0")

    }

    function configure() {
        if(!config.logger) config.logger = console;
        logger = config.logger
        if(!logger.debug) logger.debug = console.log

        _streams = {}
        if(typeof config.bus === 'string' && buses[config.bus]){
            _bus = buses[config.bus](config);
        }
        else if(typeof config.bus === 'object'){
            _bus = config.bus;
        }
        else if(typeof config.bus === 'function'){
            _bus = config.bus(config);
        }

    }
    /**
     * "register" each module in the handlers directory
     */
    function initialize() {

        if(config.handlerDirectory){
            logger.info("Registering handlers");
            requireDirectory(module, config.handlerDirectory, {visit: register});
            logger.info("Done registering handlers");
        }

    }

    /**
     * initializes the event handler and kicks off the generator for the stream
     */
    function register(handler){
        if(  EventHandlerPrototype.prototype.isPrototypeOf(handler) ){
            var key = handler.key;
            logger.info(`Registering Event: ${handler.key}` );
            if(config.autopause === true){
                logger.info(`Pausing handler: ${handler.key}`);
                handler.paused = true;
            }
            _streams[key] = handler;
            return _bus.getOrStubHighwater(handler.key)
                .then(hw => {
                    handler.maxCooldown = handler.maxCooldown || config.MAX_COOLDOWN
                    handler.minCooldown = handler.minCooldown || config.MIN_COOLDOWN
                    handler.maxRetries = handler.maxRetries || config.MAX_RETRIES
                    handler.retryBackoff = handler.retryBackoff || config.RETRY_BACKOFF
                    handler.maxConsecutiveErrors = handler.maxConsecutiveErrors || config.MAX_CONSECUTIVE_ERRORS
                    handler.state = {
                        lastId: hw,
                        cooldown: Math.floor(Math.random() * 10000), // initialize with random cooldown between 0-10 seconds
                        fetchSize: handler.fetchSize || config.FETCH_SIZE,
                        batchSize: handler.batchSize || config.BATCH_SIZE,
                        consecutiveErrors: 0
                    }
                })
                .then(handler.initialize)
                .then(function(){
                    handler.generator = generatorFactory(handler)
                    handler.loop = factory(handler);
                    handler.loop()

                });
        }
    }

    function* generatorFactory(handler){
      logger.debug("initializing generator factory for " + handler.key)
      while (1) {
          yield _bus.generator(handler)
      }
    }

    function factory(handler){
        return function loop(){

            return new Promise( (resolve, reject) => {
                // if the handler is paused then don't do any processing, just try again later
                if(handler.paused) {
                    logger.debug(`${handler.key} - PAUSED - checking again in 10 seconds`)
                    handler.cycle(10 * 1000)
                    resolve()
                    return
                }
                // sanity check to make there is no concurrent access
                if(handler.processing === true) throw new Error(`${handler.key} - Invalid state - already processing batch`)
                handler.processing = true

                iteration(handler)
                .done( () => {
                    handler.processing = false
                    logger.debug(`${handler.key} - cooldown for ${handler.state.cooldown}ms`)
                    handler.cycle(handler.state.cooldown)
                    resolve()
                })
            })
        }
    }

    function iteration(handler){
        const promise = handler.generator.next().value

         return _(promise)
        .flatMap( result => {
            logger.info(`${handler.key} - fetched ${result.records.length} results `)
            if(result.records.length === 0){
                handler.state.cooldown = Math.min(handler.state.cooldown * 2, handler.maxCooldown)
                return _([ result.maxId ])
            } else {
                handler.state.cooldown = Math.max(handler.state.cooldown / 4, handler.minCooldown)
                return processFetchedRecords(result.records, handler)
                    .stopOnError( err => {
                         handler.paused = true
                         logger.error(`${handler.key} - HALTING due to Stream Error`, JSON.stringify(err), err.stack)
                    })
            }
        })
        .flatMap(newMaxId => {
            if(newMaxId && handler.state.lastId != newMaxId){
                logger.debug(`${handler.key} - current maxId is ${handler.state.lastId}, new max id ${newMaxId}`)
                return _([ _bus.persistHighwater(handler.key, newMaxId) ])
                    .tap( () => {
                        handler.state.lastId = newMaxId
                        logger.debug(`${handler.key} - highwater updated to ${handler.state.lastId}`)
                    })
            } else {
                return _([ {} ])
            }
        })
    }

    function processFetchedRecords(records, handler){
        let streamError = null;
        let errorRows = null;

        function handleWithRetries(rows) {
          let retryCount = 0;
          let handlerErr = null;
          // convert the result of the stream to a promise so we can use setTimeout
          function handleToPromise() {
            return handler.handler(_(rows))
              .stopOnError(err => {
                handlerErr = err;
              })
              .collect()
              .toPromise(Promise)
          }
          // if there's an error, retry if we have attempts left, if not throw it. When sucessful return the result
          function handlePromiseResult(res) {
            if (handlerErr !== null && retryCount < handler.maxRetries) {
              logger.warn(`${handler.key} - error in processor on attempt ${retryCount}, backing off for ${handler.retryBackoff} before retrying. Error:`, handlerErr);
              retryCount += 1;
              handlerErr = null;
              return new Promise((resolve, reject) => {
                setTimeout(() => {
                  resolve(handleToPromise().then(handlePromiseResult));
                }, handler.retryBackoff);
              });
            } else if (handlerErr !== null) {
              logger.warn(`${handler.key} - error in processor, max number of retries exceeded. Error:`, handlerErr);
              throw handlerErr;
            } else {
              return res;
            }
          }
          return _(handleToPromise().then(handlePromiseResult));
        }

        return _(records)
            .batch(handler.batchSize)
            .flatMap( rows => {
                logger.debug(`${handler.key} - Processing batch with size ${rows.length}`);
                return handleWithRetries(rows)
                    .stopOnError( err => {
                         streamError = err;
                         errorRows = rows;
                         handler.state.consecutiveErrors += 1;
                    })
                    .collect()
                    .map( output => {
                        return { input: rows, output: output }
                    })
            })
            .flatMap( result => {
                // if the batch has errors, see if we should stop or add it to the dead letter queue
                if (streamError !== null && handler.state.consecutiveErrors >= handler.maxConsecutiveErrors) {
                  throw streamError;
                } else {
                  // if there was no error, or we used the dead letter queue take the last record's id for the new highwater
                  const newMaxId = result.input[result.input.length-1].id
                  logger.debug(`${handler.key} - input: ${result.input.length}, output: ${result.output.length}, newMaxId: ${newMaxId}`)
                  if (streamError !== null) {
                    //if there was an error, add it to the dead letter queue before returning the newMaxId
                    logger.warn(`${handler.key} - error in processor, putting events into dead letter queue`);
                    return _(_bus.addDeadletter(handler.key, errorRows)
                      .then(() => {
                        streamError = null;
                        errorRows = null;
                        return newMaxId;
                      })
                    );
                  } else {
                    // if not, reset the consecutive error count for the handler before returning the newMaxId
                    handler.state.consecutiveErrors = 0;
                    return _( [ newMaxId ] )
                  }
                }
            })
    }

    function pausedStreams(){
        return Object.keys(_streams)
            .filter( key => _streams[key].paused)
            .map( key => _streams[key])
    }

    function logPaused(){
        if(pausedStreams().length > 0){
            logger.info("Paused streams: ", pausedStreams().map(s => s.handler.key || s.handler.event  ).join(", "));
        }
    }

    function metrics(){
        return Promise.all(
            Object.keys(_streams)
            .map( key => {
                const handler = _streams[key];
                return _bus.lag(handler)
            })
        )
        .then( results => results.filter( lag => lag.metric) )
    }

    function replayDeadletter(handlerKey, deadletterId) {
      let handler = _streams[handlerKey];
      if (!handler) {
        throw new Error(`Handler ${handlerKey} does not exist`);
      }
      let deadletterError = null;
      return _bus.getDeadletter(handlerKey, deadletterId)
        .then(deadletter => {
          if (!deadletter) {
            logger.info(`${handlerKey} has no deadletter${deadletterId ? ' with id ' + deadletterId : 's'}`);
            throw new Error(`Handler ${handlerKey} has no deadletter${deadletterId ? ' with id ' + deadletterId : 's'} to process`);
          }
          logger.info(`${handlerKey} processing deadletter ${deadletter.id}`)
          return handler.handler(_(deadletter.event))
            .collect()
            .toPromise(Promise)
            .then(() => {
              logger.info(`${handlerKey} successfully processed deadletter ${deadletter.id}`);
              return _bus.removeDeadletter(deadletter.id);
            })
            .catch(err => {
              logger.error(`${handlerKey} failed to process deadletter`, err);
              throw err;
            });
        });
    }

}
