//External Dependencies
var _ = require('highland');
var http = require('request');
var Promise = require('bluebird');
Promise.promisifyAll(http);

module.exports = {
    join: join
};

// emits a tuple every X ms
function join(f, ms){
    return _(  (push, next) => {
        push(null, 'tick');
        setInterval( () => {
            push(null, 'tick');
            //next();
        }, ms);
    })
    .flatMap( () => f() )
    .latest()
}
