//gulpfile.js
var gulp = require('gulp');
var mocha = require('gulp-mocha');
var exit = require('gulp-exit');
var env = require('gulp-env');


gulp.task('watch', function() {
    gulp.watch(
        ['app/**/*.js', 'test/**/*.js'], //blurbs of files to watch
        ['mocha'] //tasks to run when the above files change
    );
});

gulp.task('mocha', ['set-test-env'], function() {
    process.env.NODE_ENV = "test";
    return gulp.src(['test/**/*.js'])
        .pipe(mocha({
            reporter: 'spec',
            globals: []
        }))
    ;
});

gulp.task('set-test-env', function () {
    env({
        NODE_ENV: "test"
    });
});

gulp.task('default', ['run', 'mocha', 'watch']);
gulp.task('test', ['mocha']);
